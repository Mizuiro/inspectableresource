﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Test : MonoBehaviour {

    // 指定したファイルを表示するUIパーツ
    [SerializeField] RawImage _image;
    [SerializeField] RawImage _listImage;
    // Resourcesフォルダ下のtextureを選択可能
    [SerializeField] Texture2DResource _resoucesTexture;
    [SerializeField] List<Texture2DResource> _textures;

    // 他にも主要なアセットの種類を指定できる
    [SerializeField] GameObjectResource _resoucesGameObject;
    [SerializeField] SpriteResource _resoucesSprite;
    [SerializeField] TextAssetResource _resourceTextAsset;
    // Objectで読んで後でキャストも可能
    [SerializeField] ObjectResource _resourceObject;

    public void Load() {
        _image.texture = _resoucesTexture.Load;
        // リストだとstringに変換出来ないので抽出する必要がある
        ResourceLoadManager.Instance.AllLoadAssetAsync<Texture2D>(_textures.Select(_ => _.GetPath).ToList(), _ => {
            foreach (var item in _.Values) {
                var clone = Instantiate(_listImage, _listImage.transform.parent);
                clone.gameObject.SetActive(true);
                clone.texture = item;
            }
        });
    }
}
