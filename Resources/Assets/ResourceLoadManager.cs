﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System.Linq;

public class ResourceLoadManager : MonoBehaviour {

    private Dictionary<string, UnityEngine.Object> _loadedResources = new Dictionary<string, UnityEngine.Object>();
    private Dictionary<string, ResourceRequest> _loadingList = new Dictionary<string, ResourceRequest>();
    private List<IDisposable> _callbackList = new List<IDisposable>();

    public IDisposable LoadAssetAsync<T>(string path, Action<T> callback) where T : UnityEngine.Object {
        var requset = Resources.LoadAsync(path);
        if (_loadedResources.ContainsKey(path)) {
            callback((T)_loadedResources[path]);
            return null;
        }
        if (_loadingList.ContainsKey(path)) {
            var dispose = _loadingList[path].ObserveEveryValueChanged(_ => _.isDone).First().Subscribe(_ => {
                callback((T)_loadingList[path].asset);
            });
            _callbackList.Add(dispose);
            return dispose;
        }
        var _dispose = requset.ObserveEveryValueChanged(_ => _.isDone).First().Subscribe(_ => {
            _loadedResources.Add(path, requset.asset);
            _loadingList.Remove(path);
            callback((T)requset.asset);
        });
        _callbackList.Add(_dispose);
        _loadingList.Add(path, Resources.LoadAsync(path));
        return _dispose;
    }

    public IDisposable AllLoadAssetAsync<T>(List<string> paths, Action<Dictionary<string, T>> callback) where T : UnityEngine.Object {
        var loadList = new Dictionary<string, T>();
        paths.ForEach(_ => {
            LoadAssetAsync<T>(_, __ => {
                loadList.Add(_, __);
            });
        });
        var dispose = Observable.EveryUpdate().Where(_ => paths.Count == loadList.Count).First().Subscribe(_ => callback(loadList));
        _callbackList.Add(dispose);
        return dispose;
    }

    public void ReleaseAssets() {
        _callbackList.ForEach(_ => {
            _.Dispose();
        });
        _loadedResources.Clear();
        _loadingList.Clear();
        Resources.UnloadUnusedAssets();
    }

    private static ResourceLoadManager instance;
    public static ResourceLoadManager Instance {
        get {
            if (instance == null) {
                instance = (ResourceLoadManager)FindObjectOfType(typeof(ResourceLoadManager));
            }

            return instance;
        }
    }

    void Awake() {
        if (Instance != this) {
            Destroy(gameObject);
            return;
        }
        instance.enabled = true;
        DontDestroyOnLoad(this.gameObject);
    }
}
