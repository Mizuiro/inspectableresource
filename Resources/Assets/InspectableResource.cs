﻿using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.IO;
using System.Linq.Expressions;
#if UNITY_EDITOR
using UnityEditor.SceneManagement;
using UnityEditor;
#endif

/// <summary>
/// BaseClass
/// </summary>
/// <typeparam name="T"></typeparam>
[System.Serializable]
public class InspectableResource<T> where T : Object {
    [SerializeField] protected string _path;

    // このクラスをString(ファイルパス)として扱えるようにすることでResources.Load等でこのクラスを渡せる
    public static implicit operator string(InspectableResource<T> data) {
        return data._path;
    }

    // 派生先のクラスで同期ロードを行う
    public virtual T Load {
        get {
            return Resources.Load<T>(_path);
        }
    }

    public string GetPath { get { return _path; } }
}

#if UNITY_EDITOR
/// <summary>
/// BaseEditorClass
/// </summary>
[CustomPropertyDrawer(typeof(InspectableResource<>))]
public class InspectableResourceEditor : PropertyDrawer {

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        SerializedProperty setedprop;
        if (property.isArray) {
            for (int i = 0; i < property.arraySize; i++) {
                var prop = property.GetArrayElementAtIndex(i);
                setedprop = SetProperty(prop);
                if (setedprop == null) {
                    continue;
                }
            }
            EditorGUI.PropertyField(position, property, label, true);
            return;
        }

        setedprop = SetProperty(property);
        if (setedprop == null) {
            return;
        }
        EditorGUI.PropertyField(position, setedprop, label);

    }

    /// <summary>
    /// セットされたオブジェクトのパスを取得して書き込む
    /// </summary>
    /// <param name="property"></param>
    /// <returns></returns>
    public SerializedProperty SetProperty(SerializedProperty property) {
        var targetProp = property.FindPropertyRelative("_resourceData");
        if (targetProp == null) {
            return null;
        }
        var path = CheckAndGetPath(targetProp.objectReferenceValue);
        property.FindPropertyRelative("_path").stringValue = path;
        if (string.IsNullOrEmpty(path)) {
            targetProp.objectReferenceValue = null;
        }
        return targetProp;
    }

    /// <summary>
    /// セットされたオブジェクトのパスを取得
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    public static string CheckAndGetPath(Object data) {
        if (data == null) {
            return "";
        }
        string path = AssetDatabase.GetAssetPath(data);
        if (!path.Contains("Resources")) {
            Debug.LogError("Resources以外のディレクトリを参照しています\n" + path);
            return "";
        }
        return ResourcesUtility.GetResourcePath(path);
    }

    /// <summary>
    /// 配列表示の時の行間を設定
    /// </summary>
    /// <param name="property"></param>
    /// <param name="label"></param>
    /// <returns></returns>
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
        float height = 0;
        if (property.isArray && property.isExpanded) {
            height += EditorGUIUtility.singleLineHeight;
            for (int i = 0; i < property.arraySize; i++) {
                height += EditorGUIUtility.singleLineHeight;
                height += 4;
            }
        }
        return EditorGUIUtility.singleLineHeight + height;
    }

}
#endif
/// <summary>
/// 実際に使用するクラスたち
/// 各アセットの種類毎にクラスを定義してインスペクターに表示可能にする(ジェネリッククラスはインスペクタに表示できない)
/// </summary>
[System.Serializable]
public class TextAssetResource : InspectableResource<TextAsset> {
#if UNITY_EDITOR
    [SerializeField] protected TextAsset _resourceData;
#endif
}

[System.Serializable]
public class SpriteResource : InspectableResource<Sprite> {
#if UNITY_EDITOR
    [SerializeField] protected Sprite _resourceData;
#endif
}

[System.Serializable]
public class Texture2DResource : InspectableResource<Texture2D> {
#if UNITY_EDITOR
    [SerializeField] protected Texture2D _resourceData;
#endif
}

[System.Serializable]
public class GameObjectResource : InspectableResource<GameObject> {
#if UNITY_EDITOR
    [SerializeField] protected GameObject _resourceData;
#endif
}

[System.Serializable]
public class AudioClipResource : InspectableResource<AudioClip> {
#if UNITY_EDITOR
    [SerializeField] protected AudioClip _resourceData;
#endif
}

[System.Serializable]
public class MaterialResource : InspectableResource<Material> {
#if UNITY_EDITOR
    [SerializeField] protected Material _resourceData;
#endif
}

[System.Serializable]
public class ObjectResource : InspectableResource<Object> {
#if UNITY_EDITOR
    [SerializeField] protected Object _resourceData;
#endif
}

#if UNITY_EDITOR
// 子クラスのエディター拡張もそれぞれ用意

[CustomPropertyDrawer(typeof(TextAssetResource))]
public class TextAssetResourceDataEditor : InspectableResourceEditor { }

[CustomPropertyDrawer(typeof(SpriteResource))]
public class SpriteResourceDataEditor : InspectableResourceEditor { }

[CustomPropertyDrawer(typeof(Texture2DResource))]
public class Texture2DResourceDataEditor : InspectableResourceEditor { }

[CustomPropertyDrawer(typeof(GameObjectResource))]
public class GameObjectResourceDataEditor : InspectableResourceEditor { }

[CustomPropertyDrawer(typeof(AudioClipResource))]
public class AudioClipResourceDataEditor : InspectableResourceEditor { }

[CustomPropertyDrawer(typeof(MaterialResource))]
public class MaterialResourceDataEditor : InspectableResourceEditor { }

[CustomPropertyDrawer(typeof(ObjectResource))]
public class ObjectResourceDataEditor : InspectableResourceEditor { }


/// <summary>
/// ファイルパスを変更してinspecterを開かない場合pathが更新されないので手動で全アセットに更新をかける用のコマンド
/// </summary>
public class ResourceReferenceChecker {
    [MenuItem("Assets/ResourcePathCheck")]
    private static void CheckAllPath() {
        AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
        var cacheScenePath = EditorSceneManager.GetActiveScene().path;
        var scenePathList = AssetDatabase.GetAllAssetPaths().Where(_ => Path.GetExtension(_) == ".unity").ToList();
        var prefabPathList = AssetDatabase.GetAllAssetPaths().Where(_ => Path.GetExtension(_) == ".prefab").ToList();
        var scriptableObjectPathList = AssetDatabase.FindAssets("t:ScriptableObject").Select(AssetDatabase.GUIDToAssetPath).ToList();
        float processed = 0;
        float count = 0;

        try {
            foreach (var path in scenePathList) {
                var scene = EditorSceneManager.OpenScene(path);
                foreach (MonoBehaviour item in Resources.FindObjectsOfTypeAll<MonoBehaviour>()) {
                    EditorUtility.DisplayProgressBar("Resource参照のチェック", "CheckingScene...", processed);
                    if (item == null) {
                        continue;
                    }
                    SetPath(new SerializedObject(item));
                }
                EditorSceneManager.SaveScene(scene);
                count += 1;
                processed = count / scenePathList.Count / 3;
            }
            EditorSceneManager.OpenScene(cacheScenePath);

            count = 0;
            foreach (var path in prefabPathList) {
                var components = AssetDatabase.LoadAssetAtPath<GameObject>(path).GetComponents<MonoBehaviour>();
                foreach (var component in components) {
                    EditorUtility.DisplayProgressBar("Resource参照のチェック", "CheckingPrefab...", processed);
                    if (component == null) {
                        continue;
                    }
                    SetPath(new SerializedObject(component));
                }
                count += 1;
                processed = (1f / 3) + (count / prefabPathList.Count / 3);
            }

            count = 0;
            foreach (var path in scriptableObjectPathList) {
                var obj = AssetDatabase.LoadAssetAtPath<ScriptableObject>(path);
                EditorUtility.DisplayProgressBar("Resource参照のチェック", "CheckingScriptableObject...", processed);
                if (obj == null) {
                    continue;
                }
                var isChanged = SetPath(new SerializedObject(obj));
                if (isChanged) {
                    EditorUtility.SetDirty(obj);
                }
                count += 1;
                processed = (1f / 3f * 2) + (count / scriptableObjectPathList.Count / 3);
            }
            Debug.Log("正常にパスの設定を完了しました");
            EditorUtility.ClearProgressBar();
            AssetDatabase.SaveAssets();
        } catch (System.Exception e) {
            Debug.LogError("例外エラーのため最後まで処理が完了しませんでした");
            Debug.LogError(e);
            EditorUtility.ClearProgressBar();
        }
    }

    /// <summary>
    /// コンポーネントが参照しているオブジェクトのパスをセットする
    /// </summary>
    /// <param name="so"></param>
    /// <returns></returns>
    static bool SetPath(SerializedObject so) {
        so.Update();
        var it = so.GetIterator();
        string beforePropPath = null;
        bool isChanged = false;
        while (it.Next(true)) {
            if (it.name.Contains("_resourceData") && it.objectReferenceValue != null) {
                var beforeProperty = so.FindProperty(beforePropPath);
                if (beforeProperty != null && beforeProperty.name.Contains("_path")) {
                    var newPath = ResourcesUtility.GetResourcePath(AssetDatabase.GetAssetPath(it.objectReferenceValue));
                    isChanged |= beforeProperty.stringValue != newPath;

                    beforeProperty.stringValue = newPath;
                    so.ApplyModifiedProperties();
                }
            }

            if (it.name.Contains("_path")) {
                beforePropPath = it.propertyPath;
            }
        }
        return isChanged;
    }
}

#endif

public class ResourcesUtility {
    public static string GetResourcePath(string path) {
        var m_path = path.Split('/');
        path = "";
        for (int i = 2; i < m_path.Length; i++) {
            path += m_path[i].Split('.')[0];
            if (i != m_path.Length - 1) {
                path += "/";
            }
        }
        return path;
    }
}
